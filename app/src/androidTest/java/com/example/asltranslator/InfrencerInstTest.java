package com.example.asltranslator;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import androidx.test.filters.SmallTest;

import com.example.asltranslator.tflite.Inferencer;

import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import static org.junit.Assert.assertEquals;

@SmallTest
public class InfrencerInstTest {

    Inferencer inferencer;
    ByteBuffer buffer;

    @Before
    public void loadInfrencer() throws IOException {
        inferencer = new Inferencer();
        InputStream model = getClass().getClassLoader().getResourceAsStream(inferencer.getModelPath());
        byte[] bytes = IOUtils.toByteArray(model);
        buffer = ByteBuffer.allocateDirect(bytes.length);
        buffer.order(ByteOrder.nativeOrder());
        buffer.put(bytes);
    }
    @Test
    public void testInfrencer_w() {
        InputStream bitmap = getClass().getClassLoader().getResourceAsStream("w.jpg");
        Bitmap image = BitmapFactory.decodeStream(bitmap);
        inferencer.testImage(image, buffer);
        assertEquals(22, inferencer.getMaxPos());
        assertEquals(0.999, inferencer.getMaxValue(), 0.01);
    }
    @Test
    public void testInfrencer_o() {
        InputStream bitmap = getClass().getClassLoader().getResourceAsStream("o.jpg");
        Bitmap image = BitmapFactory.decodeStream(bitmap);
        inferencer.testImage(image, buffer);
        assertEquals(1, inferencer.getMaxPos());
        assertEquals(0.999, inferencer.getMaxValue(), 0.01);
    }
}