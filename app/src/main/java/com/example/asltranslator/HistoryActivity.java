package com.example.asltranslator;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;

public class HistoryActivity extends AppCompatActivity {
    private List<Translation> translationList;
    private RecyclerView historyRecyclerView;
    private RecyclerView.Adapter historyAdapter;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        // Initialize RecyclerView
        historyRecyclerView = findViewById(R.id.historyRecyclerView);

        layoutManager = new LinearLayoutManager(this);
        historyRecyclerView.setLayoutManager(layoutManager);

        // Initialize list of past translations
        translationList = new ArrayList<Translation>();

        try {
            populateTranslationList();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Attach list of translations to the adapter
        historyAdapter = new HistoryAdapter(translationList);
        historyRecyclerView.setAdapter(historyAdapter);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    public void populateTranslationList() throws IOException {
        // Get list of files
        String path = getApplicationContext().getFilesDir().toString();
        File directory = new File(path);
        File[] files = directory.listFiles();

        Bitmap photo;
        String info;
        for (int i = 0; i < files.length; i+=2)
        {
            // Get the photo
            photo = BitmapFactory.decodeFile(files[i].getAbsolutePath());

            // The photo's inferencer text is always saved
            BufferedReader br = new BufferedReader(new FileReader(files[i+1]));
            info = br.readLine();

            // Add the translation
            translationList.add(new Translation(photo, info));
        }
    }
}
