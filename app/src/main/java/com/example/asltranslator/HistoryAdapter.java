package com.example.asltranslator;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.HistoryViewHolder>{
    private List<Translation> translationList;

    public static class HistoryViewHolder extends RecyclerView.ViewHolder{
        public ImageView handImage;
        public TextView translation;

        public HistoryViewHolder(View view){
            super(view);
            handImage = view.findViewById(R.id.handImage);
            translation = view.findViewById(R.id.translationText);
        }
    }

    public HistoryAdapter(List<Translation> translationListFromActivity){
        translationList = translationListFromActivity;
    }

    @NonNull
    @Override
    public HistoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.history_item, parent, false);

        HistoryViewHolder vh = new HistoryViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull HistoryViewHolder holder, int position) {
        holder.handImage.setImageBitmap(translationList.get(position).getImage());
        holder.translation.setText(translationList.get(position).getImageText());
    }

    @Override
    public int getItemCount() {
        return translationList.size();
    }
}