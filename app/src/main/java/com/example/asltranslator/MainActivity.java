package com.example.asltranslator;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import com.example.asltranslator.tflite.Inferencer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private Button btnCapture;
    private Button btnInferencer;
    private Button btnHistory;
    private ImageView imgCapture;
    private Bitmap bp;
    private Inferencer inferencer;
    private Uri photoURI;
    private String photoPath;
    private static final int Image_Capture_Code = 1;
    private float x1, x2, y1, y2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Log.d("Main",getFilesDir().toString());

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnCapture =(Button)findViewById(R.id.btnTakePicture);
        imgCapture = (ImageView) findViewById(R.id.capturedImage);
        btnInferencer = (Button)findViewById(R.id.btnInferencer);
        btnHistory = (Button)findViewById(R.id.btnHistory);

        btnHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), HistoryActivity.class);
                startActivity(intent);
            }
        });

        btnCapture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cInt = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                if(cInt.resolveActivity(getPackageManager()) != null){
                    File photo = null;
                    Bitmap rotatedPhoto = null;
                    try{
                        photo = createImageFile();
                    } catch (IOException ex){
                        Context context = getApplicationContext();
                        CharSequence text = "Could not create file.";
                        int duration = Toast.LENGTH_LONG;
                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();
                    }

                    if (photo != null) {
                        photoURI = FileProvider.getUriForFile(MainActivity.this,
                                "com.example.asltranslator.android.fileprovider",
                                photo);
                        cInt.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                        startActivityForResult(cInt, Image_Capture_Code);
                    }
                }
            }
        });

        btnInferencer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inferencer = new Inferencer();
                try {
                    inferencer.testImage(bp,inferencer.loadModelFile((MainActivity.this)));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Context context = getApplicationContext();
                CharSequence text = "Strongest Match Pos: " + inferencer.getMaxPos() + " Strongest Match Value: " + inferencer.getMaxValue();
                int duration = Toast.LENGTH_LONG;
                Toast toast = Toast.makeText(context, text, duration);
                toast.show();

                try {
                    writeTranslationData(bp, text.toString());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Image_Capture_Code) {
            if (resultCode == RESULT_OK) {
                //bp = (Bitmap) data.getExtras().get("data");
                //imgCapture.setImageBitmap(bp);

                //Uri imageUri = data.getData();
                try {
                    bp = MediaStore.Images.Media.getBitmap(this.getContentResolver(), photoURI);
                    imgCapture.setImageBitmap(bp);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show();
            }
        }
    }

    //TODO: Comment, pare
    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        return image;
    }

    public boolean onTouchEvent(MotionEvent touchEvent){
        switch(touchEvent.getAction()){
            case MotionEvent.ACTION_DOWN:
                x1 = touchEvent.getX();
                y1 = touchEvent.getY();
                break;
            case MotionEvent.ACTION_UP:
                x2 = touchEvent.getX();
                y2 = touchEvent.getY();
                if(x1 - x2 < 50 && x1 - x2 != 0){
                    Intent i = new Intent(MainActivity.this, HistoryActivity.class);
                    startActivity(i);
                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                }else if(x1 - x2 > 50){
                    Intent cInt = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                    if(cInt.resolveActivity(getPackageManager()) != null) {
                        File photo = null;

                        try {
                            photo = createImageFile();
                        } catch (IOException ex) {
                            Context context = getApplicationContext();
                            CharSequence text = "Could not create file.";
                            int duration = Toast.LENGTH_LONG;
                            Toast toast = Toast.makeText(context, text, duration);
                            toast.show();
                        }

                        if (photo != null) {
                            photoURI = FileProvider.getUriForFile(MainActivity.this,
                                    "com.example.asltranslator.android.fileprovider",
                                    photo);
                            cInt.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                            startActivityForResult(cInt, Image_Capture_Code);
                        }
                    }
//                    Intent i = new Intent(MainActivity.this, SwipeRight.class);
//                    startActivity(i);
                }
            break;
        }
        return false;
    }

    public void writeTranslationData(Bitmap photo, String photoInfo) throws IOException {
        /* Construct file name
         * The naming conventions for the pictures and the inferencer data
         * ensures that files are written to tbe directory in chronological
         * order, prevents collisions, and couples the pictures and the
         * inferencer data through their name and by extension, their location
         * in the directory.
         */

        String fileName = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss").format(new Date());
        fileName = "JPEG_" + fileName;

        //Write photo
        FileOutputStream fos = getApplicationContext().openFileOutput(fileName, Context.MODE_PRIVATE);
        photo.compress(Bitmap.CompressFormat.PNG, 100, fos);
        fos.close();

        //Write photoinfo
        fos = getApplicationContext().openFileOutput(fileName + "_INFO", Context.MODE_PRIVATE);
        fos.write(photoInfo.getBytes());
        fos.close();
    }

    public static Bitmap handleSamplingAndRotationBitmap(Context context, Uri selectedImage) throws IOException {
        int MAX_HEIGHT = 1024;
        int MAX_WIDTH = 1024;

        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        InputStream imageStream = context.getContentResolver().openInputStream(selectedImage);
        BitmapFactory.decodeStream(imageStream, null, options);
        imageStream.close();

        options.inSampleSize = calculateInSampleSize(options, MAX_WIDTH, MAX_HEIGHT);

        options.inJustDecodeBounds = false;
        imageStream = context.getContentResolver().openInputStream(selectedImage);
        Bitmap img = BitmapFactory.decodeStream(imageStream, null, options);
        img = rotateImageIfRequired(context, img, selectedImage);
        return img;
    }

    private static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight){
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if(height > reqHeight || width > reqWidth){
            final int heightRatio = Math.round((float)height / (float)reqHeight);
            final int widthRatio = Math.round((float)width / (float)reqHeight);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
            final float totalPixels = width * height;
            final float totalReqPixelsCap = reqWidth * reqHeight * 2;
            while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap){
                inSampleSize++;
            }
        }
        return inSampleSize;
    }

    private static Bitmap rotateImageIfRequired(Context context, Bitmap img, Uri selectedImage) throws IOException {
        InputStream input = context.getContentResolver().openInputStream(selectedImage);
        ExifInterface ei;
        if(Build.VERSION.SDK_INT > 23){
            ei = new ExifInterface(input);
        } else {
            ei = new ExifInterface(selectedImage.getPath());
        }
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
        switch(orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotateImage(img, 90);
            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotateImage(img, 180);
            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotateImage(img, 270);
            default:
                return img;
        }
    }

    private static Bitmap rotateImage(Bitmap img, int degree){
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img,0,0,img.getWidth(),img.getHeight(),matrix,true);
        img.recycle();
        return rotatedImg;
    }
}
