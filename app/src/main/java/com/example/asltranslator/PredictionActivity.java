package com.example.asltranslator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.example.asltranslator.tflite.Inferencer;

public class PredictionActivity extends AppCompatActivity {

    private Bitmap bp;
    private Button btnRunInferencer;
    private Inferencer inferencer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prediction);
        Intent intent = getIntent();
        bp = (Bitmap) intent.getParcelableExtra("BitmapImage");
    }
}
