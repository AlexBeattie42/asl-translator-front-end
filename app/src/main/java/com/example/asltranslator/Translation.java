package com.example.asltranslator;

import android.graphics.Bitmap;

public class Translation {
    // This is a simple wrapper class to pair an Image with
    // the data the inferencer provides for it

    private Bitmap image;
    private String inferencerData;

    public Translation(Bitmap bitmapPhoto, String photoInfo){
        image = bitmapPhoto;
        inferencerData = photoInfo;
    }

    public Bitmap getImage() {
        return image;
    }

    public String getImageText() {
        return inferencerData;
    }
}