package com.example.asltranslator.tflite;

import android.app.Activity;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.graphics.Bitmap;

import org.tensorflow.lite.DataType;
import org.tensorflow.lite.Interpreter;
import org.tensorflow.lite.support.common.FileUtil;
import org.tensorflow.lite.support.image.ImageProcessor;
import org.tensorflow.lite.support.image.TensorImage;
import org.tensorflow.lite.support.image.ops.ResizeOp;
import org.tensorflow.lite.support.tensorbuffer.TensorBuffer;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

public class Inferencer {

    private int maxPos = 0;
    private float maxValue= 0;
    private TensorBuffer probabilityBuffer;

    public Inferencer() {
        probabilityBuffer = TensorBuffer.createFixedSize(new int[]{1,26}, DataType.FLOAT32);
    }

    public void testImage(Bitmap image, ByteBuffer modelBuffer){
        ImageProcessor imageProcessor = new ImageProcessor.Builder()
                .add(new ResizeOp(300, 300, ResizeOp.ResizeMethod.BILINEAR))
                .build();

        TensorImage tImage = new TensorImage(DataType.FLOAT32);

        tImage.load(image);
        tImage = imageProcessor.process(tImage);

        Interpreter tflite = new Interpreter(modelBuffer);
        tflite.run(tImage.getBuffer(), probabilityBuffer.getBuffer());
        getStrongestMatch(probabilityBuffer.getFloatArray());
    }

    public MappedByteBuffer loadModelFile(Activity activity) throws IOException {
        AssetFileDescriptor fileDescriptor = activity.getAssets().openFd(getModelPath());
        FileInputStream inputStream = new FileInputStream(fileDescriptor.getFileDescriptor());
        FileChannel fileChannel = inputStream.getChannel();
        long startOffset = fileDescriptor.getStartOffset();
        long declaredLength = fileDescriptor.getDeclaredLength();
        return fileChannel.map(FileChannel.MapMode.READ_ONLY, startOffset, declaredLength);
    }

    private void getStrongestMatch(float[] inputArray) {
        maxValue = inputArray[0];
        maxPos = 0;
        for(int i=1;i < inputArray.length;i++){
            if(inputArray[i] > maxValue){
                maxValue = inputArray[i];
                maxPos = i;
            }
        }
    }

    public String getModelPath(){
        return "converted_model.tflite";
    }

    public int getMaxPos() {
        return maxPos;
    }

    public float getMaxValue() {
        return maxValue;
    }

    public TensorBuffer getProbabilityBuffer() {
        return probabilityBuffer;
    }
}