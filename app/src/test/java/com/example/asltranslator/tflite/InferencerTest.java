package com.example.asltranslator.tflite;

import org.junit.Assert;
import org.junit.Test;
import org.tensorflow.lite.DataType;
import org.tensorflow.lite.support.tensorbuffer.TensorBuffer;

import java.io.InputStream;

import static org.junit.Assert.*;

public class InferencerTest {

    @Test
    public void testImage() {
        Inferencer inferencer = new Inferencer();
       InputStream is = getClass().getClassLoader().getResourceAsStream(inferencer.getModelPath());

    }

    @Test
    public void getMaxPos() {
        Inferencer inferencer = new Inferencer();
        assertEquals(0,inferencer.getMaxPos());
    }

    @Test
    public void getMaxValue() {
        Inferencer inferencer = new Inferencer();
        assertEquals(0,inferencer.getMaxValue(),0);
    }

    @Test
    public void getProbabilityBuffer() {
        Inferencer inferencer = new Inferencer();
        TensorBuffer probabilityBuffer = TensorBuffer.createFixedSize(new int[]{1,26}, DataType.FLOAT32);
        // Check if the two buffers are the same class type
        assertEquals(probabilityBuffer.getClass(), inferencer.getProbabilityBuffer().getClass());
    }
}